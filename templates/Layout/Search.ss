<main class="main" style="scroll-behavior: smooth">
    <div id="collection-content" class="page-content mb-6 pb-6">
        <div class="container-fluid">
            <div class="row gutter-lg main-content-wrap">
                <aside class="col-lg-3 sidebar sidebar-fixed shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay">
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    </div>
                    <div class="sidebar-content">
                        <form method="get" role="form" id="filter-form">
                            <div class="sticky-sidebar" data-sticky-options="{'top': 10}">
                                 <div class="widget">
                                    <h3 class="widget-title">Search</h3>
                                    <div class="widget-body">
                                        <input class="form-control" type="text" placeholder="Search by Name, SKU,..."
                                               value="$getSearchTerm" name="search" />
                                    </div>
                                </div>
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title">Price</h3>
                                    <div class="widget-body filter-items filter-price">
                                        <label for="price_min">Min $SiteSettings.CurrencySymbol()</label>
                                        <div class="input-group">
                                             <input class="form-control" type="number" name="price_min" data-role="min_val" value="$getOptions().PriceRange.Min" />
                                        </div>
                                        <br class="d-md-none"/>
                                        <label for="price_max">Max $SiteSettings.CurrencySymbol()</label>
                                        <div class="input-group">
                                            <input class="form-control" type="number" name="price_max"
                                                   data-role="max_val" value="$getOptions().PriceRange.Max" />
                                        </div>
                                    </div>
                                </div>
                                <% loop $getOptions().SearchFilters %>
                                    <% if $Type == 'color' %>
                                        <div class="widget pb-3">
                                            <h3 class="widget-title">$Name</h3>
                                            <div class="row color-swatch">
                                                <% loop $SearchData %>
                                                    <div class="col-1 pr-3">
                                                        <label for="$ID">
                                                            <input type="checkbox" name="search_data[]" value="$ID"
                                                                   id="$ID" <% if $Selected %>checked<% end_if %> />
                                                            <span class="swatch" style="background: #{$Value}"></span>
                                                        </label>
                                                    </div>
                                                <% end_loop %>
                                            </div>
                                        </div>
                                    <% else %>
                                        <div class="widget widget-collapsible">
                                            <h3 class="widget-title collapsed">$Name</h3>
                                            <ul class="widget-body filter-items" style="display:none">
                                                <% loop $SearchData %>
                                                    <li class="pl-0">
                                                        <label for="$ID">
                                                            <input type="checkbox" name="search_data[]" value="$ID"
                                                            id="$ID" <% if $Selected %>checked<% end_if %> />
                                                            $Value<span>($Matches)</span>
                                                        </label>
                                                    </li>
                                                <% end_loop %>
                                            </ul>
                                        </div>
                                    <% end_if %>
                                <% end_loop %>
                                <div class="row pb-2 mt-2">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-sm btn-primary">Filter</button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <a href="$AbsoluteLink" class="btn btn-sm btn-outline-secondary">Clear</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </aside>
                <div class="col-lg-9 main-content">
                    <nav class="col-11 toolbox sticky-toolbox sticky-content fix-top">
                        <div class="toolbox-left">
                            <a href="#"
                                class="toolbox-item left-sidebar-toggle btn btn-sm btn-outline btn-primary
                                    btn-rounded btn-icon-right d-lg-none">Filter<i
                                    class="d-icon-arrow-right"></i></a>
                            <div class="toolbox-item toolbox-sort select-box text-dark">
                                <label>Sort By :</label>
                                <select name="orderby" class="form-control">
                                    <% loop $Top.getOptions().Sort %>
                                        <option value="$FilterLink" <% if $Selected %>selected="selected"<% end_if %>>
                                            $Option
                                        </option>
                                    <% end_loop %>
                                </select>
                            </div>
                        </div>
                    </nav>
                    <div class="row cols-2 product-wrapper cols-md-4">
                        <% loop $PaginatedResults(24) %>
                            <% include ProductCard Product=$Me %>
                        <% end_loop %>
                    </div>
                    <% if PaginatedResults(24).MoreThanOnePage %>
                        <% with PaginatedResults(24) %>
                        <nav class="toolbox toolbox-pagination">
                            <ul class="pagination">
                                <% if $NotFirstPage %>
                                <li class="page-item">
                                    <a class="page-link page-link-prev" href="$PrevLink" aria-label="Previous" tabindex="-1"
                                       aria-disabled="true">
                                        <i class="d-icon-arrow-left"></i>Prev
                                    </a>
                                </li>
                                <% else %>
                                <li class="page-item disabled">
                                    <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
                                       aria-disabled="true">
                                        <i class="d-icon-arrow-left"></i>Prev
                                    </a>
                                </li>
                                <% end_if %>
                                <% loop $PaginationSummary %>
                                    <% if $CurrentBool %>
                                        <li class="page-item active" aria-current="page"><a class="page-link" href="#">$PageNum</a>
                                    <% else %>
                                        <% if $Link %>
                                            <li class="page-item"><a class="page-link" href="$Link">$PageNum</a></li>
                                        <% else %>
                                            <li class="page-item">...</li>
                                        <% end_if %>
                                    <% end_if %>
                                <% end_loop %>
                                <% if $NotLastPage %>
                                <li class="page-item">
                                    <a class="page-link page-link-next" href="$NextLink" aria-label="Next">
                                        Next<i class="d-icon-arrow-right"></i>
                                    </a>
                                </li>
                                <% else %>
                                <li class="page-item disabled">
                                    <a class="page-link page-link-next" href="#" aria-label="Next" tabindex="-1" aria-disabled="true">
                                        Next<i class="d-icon-arrow-right"></i>
                                    </a>
                                </li>
                                <% end_if %>
                            </ul>
                        </nav>
                        <% end_with %>
                    <% end_if %>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Scroll Top -->
<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="d-icon-arrow-up"></i></a>