<main class="main">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="/"><i class="d-icon-home"></i></a></li>
                <li><a href="$Blog.AbsoluteLink" class="active">Blog</a></li>
                <li>$Title</li>
            </ul>
        </div>
    </nav>
    <div class="page-content mt-6">
        <div class="container">
            <div class="row gutter-lg">
                <div class="col">
                    <article class="post-single">
                        <figure class="post-media">
                            <% if $Image %>
                                <img src="$Image.Fill(870,420).AbsoluteLink" alt="$Title" width="870" height="420" loading="lazy" />
                            <% else %>
                                <img src="$SiteSettings.Logo.Fill(870,420).AbsoluteLink" alt="$Title" width="870" height="420" loading="lazy" />
                            <% end_if %>
                        </figure>
                        <div class="post-details">
                            <div class="post-meta"><a href="#" class="post-date">$Created.format('MMM d Y')</a></div>
                            <h4 class="post-title"><a href="$AbsoluteLink">$Title</a></h4>
                            <div class="post-body mb-7">$Content</div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</main>