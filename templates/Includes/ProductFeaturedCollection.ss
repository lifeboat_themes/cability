<% if $Collection.Image %>
<a href="$Collection.AbsoluteLink">
    <div class="intro-banner1 banner banner-fixed">
        <figure>
            <img src="$Collection.Image.AbsoluteURL" alt="$Collection.Title" width="280" height="auto" loading="lazy" />
        </figure>
    </div>
</a>
<% end_if %>