<!DOCTYPE html>
<!--[if !IE]><!-->
<html lang="$ContentLocale">
<!--<![endif]-->
<!--[if IE 6 ]><html lang="$ContentLocale" class="ie ie6"><![endif]-->
<!--[if IE 7 ]><html lang="$ContentLocale" class="ie ie7"><![endif]-->
<!--[if IE 8 ]><html lang="$ContentLocale" class="ie ie8"><![endif]-->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>

    <link rel="stylesheet" type="text/css" href="$StaticFile('vendor/fontawesome-free/css/all.min.css')" />
    <link rel="stylesheet" type="text/css" href="$StaticFile('vendor/owl-carousel/owl.carousel.min.css')" />
    <link rel="stylesheet" type="text/css" href="$StaticFile('css/style.min.css')" />
</head>
<body>
    <% include Header %>

    <div class="page-wrapper">
        $Layout
    </div>

    <% include Footer %>

    <div class="mobile-menu-wrapper">
        <div class="mobile-menu-overlay">
        </div>
        <a class="mobile-menu-close" href="#"><i class="d-icon-times"></i></a>
        <div class="mobile-menu-container scrollable">
            <ul class="mobile-menu mmenu-anim">
                <li>
                    <a href="/" class="py-0">
                        <img src="$SiteSettings.Logo.Fit(163,39).AbsoluteLink" alt="$SiteSettings.Title logo" loading="lazy"
                            width="$SiteSettings.Logo.Fit(163,39).getWidth" height="$SiteSettings.Logo.Fit(163,39).getHeight" />
                    </a>
                </li>
            </ul>
            <form action="/search" class="input-wrapper">
                <input type="text" class="form-control" name="search" autocomplete="off"
                       placeholder="Search your keyword..." />
                <button class="btn btn-search" type="submit">
                    <i class="d-icon-search"></i>
                </button>
            </form>
            <ul class="mobile-menu mmenu-anim">
                <% loop $SiteSettings.MainMenu.MenuItems %>
                    <% if $Children.count %>
                        <li>
                            <a href="#">$Title</a>
                            <ul>
                                <% loop $Children %>
                                    <li><a href="$Link">$Title</a></li>
                                <% end_loop %>
                            </ul>
                        </li>
                    <% else %>
                        <li <% if isCurrent %>class="active"<% end_if %>>
                            <a href="$Link">$Title</a>
                        </li>
                    <% end_if %>
                <% end_loop %>
            </ul>
        </div>
    </div>

    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:300,400,500,600,700,800', 'Oswald:300,400,500,600,700' ] }
        };
    </script>
    <script type="text/javascript" src="$StaticFile('js/webfont.js')" async></script>
    <script src="$StaticFile('vendor/jquery/jquery.min.js')"></script>
    <script src="$StaticFile('vendor/sticky/sticky.min.js')"></script>
    <script src="$StaticFile('vendor/imagesloaded/imagesloaded.pkgd.min.js')"></script>
    <script src="$StaticFile('vendor/elevatezoom/jquery.elevatezoom.min.js')"></script>
    <script src="$StaticFile('vendor/owl-carousel/owl.carousel.min.js')"></script>
    <script src="$StaticFile('vendor/parallax/parallax.min.js')"></script>
    <script src="$StaticFile('js/main.min.js')"></script>
    <script src="$StaticFile('js/product.min.js')"></script>
    $SystemAdditions
</body>
</html>