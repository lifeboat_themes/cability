# Cability

## Home page Custom Fields

### Home: Featured Collection 1/2/3
You can select up to 3 collections to be featured in the home page banner.

### Home: Parallax Collection
You can select 1 collection to be featured in the parallax section.

### Newsletter Section Image
You can select an image to be the background of the newsletter section, if you have the newsletter enabled

### Featured Items Section Title
You can customise the title for the featured items section, where the default title is 'Featured Items'.
This is displayed in the product page on the right sidebar.

### Best Sellers Section Title
You can customise the title for the featured items section, where the default title is 'Best Sellers'.
This is displayed in the home page underneath the parallax collection.

### Product Page featured Collection (In sidebar)
You can select a collection to be featured on the right in a sidebar when viewing a single product.

### Footer: First Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the left of the two menu options.

_Note: Submenus items will not be displayed_

### Footer: Second Menu
You can select a menu to be displayed in the footer middle section. This will be the menu shown on the right of the two menu options.

_Note: Submenus items will not be displayed_

### Product: Tags
You can use up to two (2) tags for each product (case-sensitive):

- featured: The featured tag can be used for up to four (4) products to be shown in the featured section in the sidebar of a single product page.
- best_seller: The best_seller tag can be used for up to four (4) products to be shown in the best-sellers section underneath the parallax section in the home page.

## Collection
### Tags
You can use up to one (1) tag for each collection (case-sensitive):

- show_in_home: This tag can be used to show up to four (4) collections in the home page underneath the banner section.

### Custom Fields

#### Collection Banner
You can select an Image to be shown as a banner in the selected collection page.

#### Banner Image
You can select an Image to be shown as a banner in the home page, if the collection is selected to be featured in the home page banner from the design section custom fields.

#### Banner Heading
If the collection is selected to be shown as a banner in the home page, from the design section custom fields, you can have a custom heading for each selected collection, which can be set here.

#### Featured Image
You can set an Image to be shown in the home page if this collection is selected as featured using the 'featured' tag.

#### Parallax Image
If the collection is selected to be shown as the parallax collection, you can set an image to be displayed in the parallax section.

